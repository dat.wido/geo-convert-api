FROM node:14-alpine
ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV
ENV PORT 8080

WORKDIR /app

COPY . . 
# COPY --from=builder /app/dist/ dist/
# COPY metadata metadata
# COPY migrations migrations
# COPY package.json yarn.lock ./

RUN yarn install && yarn cache clean

HEALTHCHECK --interval=60s --timeout=2s --retries=3 CMD wget localhost:${PORT}/healthz -q -O - > /dev/null 2>&1

EXPOSE $PORT
CMD ["yarn", "start"]