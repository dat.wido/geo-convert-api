# PROJECT
Geo Convert API 
Nodejs express api service get input amazone s3 storage and conver to another driver file, then upload to output amazone s3 storage

## REQUIRED
- docker  
- amazon s3 storage  

## USING
- 

## RUN DEVELOP
run docker-compose  
$ docker-compose -f docker-compose.yml -f docker-compose.dev.yml up -d  

install dependencies  
$ docker-compose exec convert-api yarn -it  

## BUILD
login registry  
$ docker login <registry>  

build image  
$ docker build -t <registry-url> .  

when check ok add version tag to image and push to registry  (add changelog if need)  
$ docker tag <registry-url> <registry-url>[:TAG]  
$ docker push <registry-url>[:TAG]  


## CHANGE LOG
24-10-2020: init project.  

## NOTE
