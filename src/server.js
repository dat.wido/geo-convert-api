'use strict'

const bodyParser = require('body-parser')
const express = require('express')
const ogr2ogr = require('ogr2ogr')
const fs = require('fs')
var AWS = require('aws-sdk')

var s3 = new AWS.S3({
    accessKeyId: process.env.S3_ACCESS_KEY_ID,
    secretAccessKey: process.env.S3_SECRET_KEY,
    endpoint: process.env.S3_ENDPOINT,
    s3ForcePathStyle: true, // needed with minio?
    signatureVersion: 'v4'
});

function enableCors(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'POST')
    res.header('Access-Control-Allow-Headers', 'X-Requested-With')
    res.header('Access-Control-Expose-Headers', 'Content-Disposition')
    next()
}

// Constants
const PORT = process.env.PORT || 8080
const HOST = '0.0.0.0'

// App
const app = express()
app.use(enableCors)
// Permit the app to parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// Use body-parser as middleware for the app.
app.use(bodyParser.json())

app.post('/convert', async (req, res) => {
    console.log('[Request Time]:', Date.now(), req.path, req.body)

    const {
        srcPath,
        desPath,
        desDriver,
        srcSrs,
        desSrs,
    } = req.body

    // validate path
    if (!srcPath || !desPath) {
        res.status(400).json({ error: true, msg: 'No part provided' })
        return
    }

    // download origin file
    const tmp_origin = '/tmp/' + new Date().getTime() + "_origin_file"
    let download_process = new Promise((resolve, rejects) => {
        const origin_params = { Bucket: process.env.S3_BUCKET, Key: srcPath }
        let origin_file = require('fs').createWriteStream(tmp_origin)

        s3.getObject(origin_params)
            .on('httpData', chunk => origin_file.write(chunk))
            .on('httpDone', () => { origin_file.end(); resolve(); })
            .on('error', err => rejects(err))
            .send();
    })

    await download_process

    let converFile = ogr2ogr(tmp_origin)
        .onStderr((data) => { console.error(data) })
        .options(["--config", "SHAPE_RESTORE_SHX", "TRUE"])
        .stream()

    const tmp_conver = '/tmp/' + new Date().getTime() + "_conver_file"

    converFile.pipe(fs.createWriteStream(tmp_conver))

    // fs.unlink(tmp_origin);

    return res.json({
        "message": "ok babe"
    })
})

app.listen(PORT, HOST)
console.log(`Running on http://${HOST}:${PORT}`)